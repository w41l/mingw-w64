#!/bin/sh

cd $(dirname $0); CWD=$(pwd);

PKGBASE="mingw-w64"
FORCE=${FORCE:-"NO"}
if [ "$FORCE" = "YES" ]; then
  REINSTALL="--reinstall"
fi

BUILD_TYPE=${BUILD_TYPE:-""}

helpusage() {
echo "Usage: $(basename $0) 0|1 or 2 <PACKAGE_NAME>
OPTIONS:
 0 - Full bootstrapping
 1 - Rebuild or upgrade installed packages except binutils
 2 - Build package <PACKAGE_NAME>
"
}

failedrun() {
  RETVAL=$?
  if [ $RETVAL -gt 0 ]; then
    echo "==> Failed package $p"
    exit 1
  fi
}

run_build() {
  if [ -r $p/${p}.SlackBuild ]; then
    cd $p
    n=$(PRINT_PACKAGE_NAME=1 sh ${p}.SlackBuild)
    echo "==> $n"
    sh ${p}.SlackBuild || failedrun
    if [ "$x" = "headers" ]; then
      for h in headers headers-bootstrap; do
        b=$(echo $n | rev | cut -d '-' -f -3 | rev)
        n=$(echo $b | sed "s/^/$PKGBASE-$h-/g")
        cd $OUTPUT
        /sbin/upgradepkg --install-new $REINSTALL $n
        cd $CWD
      done
    elif [ "$x" = "winpthreads" ]; then
      if test -f $(echo /var/lib/pkgtools/packages/$PKGBASE-headers-bootstrap-*); then
        /sbin/removepkg $PKGBASE-headers-bootstrap
      fi
      cd $OUTPUT
      /sbin/upgradepkg --install-new $REINSTALL $n
      cd $CWD
    elif [ "$x" = "gcc" ]; then
      if test -f $(echo /var/lib/pkgtools/packages/$PKGBASE-gcc-static-*); then
        /sbin/removepkg $PKGBASE-gcc-static
      fi
      cd $OUTPUT
      /sbin/upgradepkg --install-new $REINSTALL $n
      cd $CWD
    else
      cd $OUTPUT
      /sbin/upgradepkg --install-new $REINSTALL $n
      cd $CWD
    fi
  else
    echo "ERROR: Could not found package $x"
    exit 1
  fi
}

case "$1" in
  0)
    LIST="binutils headers gcc-static crt winpthreads gcc"
    ;;
  1)
    LIST="gcc-static crt winpthreads gcc"
    echo "==> Upgrade/rebuild mingw-w64 or gcc"
    ;;
  2)
    if [ -z "$2" ]; then
      echo "Usage: $(basename $0) <PACKAGE_NAME>"
      exit 1
    else
      LIST="$2"
    fi
    ;;
  *)
    helpusage
    ;;
esac

for x in $(echo "$LIST"); do
  p="$PKGBASE-$x"
  run_build
  unset n
  unset p
done
