#!/bin/sh
# $Id: 32dev.sh,v 1.4 2014/10/02 09:31:37 eha Exp eha $
# Copyright (C) 2007  Frederick Emmott <mail@fredemmott.co.uk>
# Copyright 2009  Eric Hameleers, Eindhoven, NL
# Based on the file with the same name which is part of
# the Slamd64 Linux project (www.slamd64.com)

# Distributed under the GNU General Public License, version 2, as
# published by the Free Software Foundation.

# Mingw SYSROOT
MINGWSYSROOT=@MINGWSYSROOT@

# Mingw build tools
MINGWTOOLS=@MINGWTOOLS@

# Mingw ROOT
MINGWROOT=@MINGWROOT@

# Modify the compilation/linking environment:
PATH_HOLD="$PATH"
export PATH="$PATH_HOLD:@MINGWTOOLS@/bin"

# Change the shell prompt to make it clear that we are in 32bit mode:
PS1='\u@\h (mingw32):\w\$ '
