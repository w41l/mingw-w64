if [ -f @MINGWROOT@/@TARGET@/include/pthread_unistd.h ]; then
  if ls -A var/log/packages/mingw-w64-headers-bootstrap-* >/dev/null; then
    echo
    echo " *WARNING*: Please run:
      removepkg mingw-w64-headers-bootstrap
    then reinstall this package!"
    echo
  fi
fi
