#!/bin/bash

# Copyright 2018-2024  Widya Walesa <pkgs.w41l@walecha.web.id>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0); CWD=$(pwd);

SRCNAM=pthreads4w
PKGNAM=mingw-w64-$SRCNAM
VERSION=${VERSION:-3.0.0}
GITVERSION="07053a521b0a9deb6db2a649cde1f828f2eb1f4f"
BUILD=${BUILD:-1}
TAG=${TAG:-_wls}
PKGTYPE=${PKGTYPE:-"tgz"}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i686 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PKGNAM-$VERSION-$ARCH-${BUILD}${TAG}.$PKGTYPE"
  exit 0
fi

if [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=pentium4 -mtune=generic"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
  LIB_ARCH=s390
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -march=x86-64 -mtune=generic -fPIC"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "armv7hl" ]; then
  SLKCFLAGS="-O3 -march=armv7-a -mfpu=vfpv3-d16"
  LIBDIRSUFFIX=""
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

case "$ARCH" in
    arm*) TARGET=${ARCH}-w64-mingw32-gnueabi ;;
    *)    TARGET=${ARCH}-w64-mingw32 ;;
esac

WRK=${WRK:-"/tmp/wlsbuild"}
PKG="$WRK/package-$PKGNAM"
OUTPUT=${OUTPUT:-"/tmp"}

set -e
# Uncomment these error options to debug build process
#set -o errtrace # -E ERR trap is inherited by shell functions.
#set -o errexit  # -e Exit immediately if a command exits with a non-zero status.
#set -o pipefail # Return exit status of last command in pipe that is non-zero.
#set -o nounset  # -u Treat unset variables as an error when substituting.

if [ ! -r /etc/profile.d/mingw.sh ]; then
  echo "** Please (re)install mingw-w64-binutils first!"
  exit 1
fi

. /etc/profile.d/mingw.sh

if [ -f $MINGWROOT/$TARGET/lib/libwinpthread-1.dll ]; then
  echo "** ERROR: Threading library winpthreads already installed"
  exit 1
fi

MULTILIB=${MULTILIB:-false}
if [ "$ARCH" = "x86_64" -a -L /lib/ld-linux.so.2 ]; then
  if [ $MULTILIB == true ]; then
    TARGET32="i686-w64-mingw32"
    echo "* build with multilib targets: $TARGET, $TARGET32"
  else
    MULTILIB=false
  fi
else
  MULTILIB=false
  echo "* multilib or dual lib build only available in ARCH x86_64"
fi

echo "* mingw-w64 SYSROOT: $MINGWSYSROOT"
echo "* mingw-w64 ROOT: $MINGWROOT"
echo "* mingw-w64 TOOLS: $MINGWTOOLS"
sleep 3

set -e
# Uncomment these error options to debug build process
#set -o errtrace # -E ERR trap is inherited by shell functions.
#set -o errexit  # -e Exit immediately if a command exits with a non-zero status.
#set -o pipefail # Return exit status of last command in pipe that is non-zero.
#set -o nounset  # -u Treat unset variables as an error when substituting.

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT

cd $WRK
rm -rf $SRCNAM-code-$GITVERSION
unzip $CWD/$SRCNAM-code-v$VERSION.zip || exit 1
cd $SRCNAM-code-$GITVERSION || exit 1

# Fix perms/owners:
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 754 \) -exec chmod 755 {} \+ \
  -o \( -perm 664 \) -exec chmod 644 {} \+

# End of preparations
if echo "$*" | grep -qw -- --prep ; then
  exit 0
fi

autoreconf -ivsf

cd $WRK
BUILDDIR="$SRCNAM-build"
rm -rf $BUILDDIR
mkdir -p $BUILDDIR
cd $BUILDDIR

DEFOPTS="--enable-static \
--enable-shared \
--prefix=$MINGWROOT/$TARGET \
--host=$TARGET"

if [ $MULTILIB == true ]; then
  echo "* ==============================================================="
  echo "* building target ${TARGET32}"
  echo "* ==============================================================="
  sleep 1
  mkdir -p ${PKG}${MINGWROOT}/$TARGET/lib32
  RC="$TARGET-windres -F pe-i386" \
  DLLTOOL="$TARGET-dlltool -m i386" \
  ../$SRCNAM-code-$GITVERSION/configure \
    --target=$TARGET32 \
    $DEFOPTS

  make -j1 realclean GC ARCH="-m32"
  cp -a libpthreadGC3.dll.a pthreadGC3.dll ${PKG}${MINGWROOT}/$TARGET/lib32
  mv ${PKG}${MINGWROOT}/$TARGET/lib32/libpthreadGC3.dll.a ${PKG}${MINGWROOT}/$TARGET/lib32/libpthread.dll.a
  mv ${PKG}${MINGWROOT}/$TARGET/lib32/pthreadGC3.dll ${PKG}${MINGWROOT}/$TARGET/lib32/pthread.dll

  make -j1 realclean GC-static ARCH="-m32"
  cp -a libpthreadGC3.a ${PKG}${MINGWROOT}/$TARGET/lib32
  mv ${PKG}${MINGWROOT}/$TARGET/lib32/libpthreadGC3.a ${PKG}${MINGWROOT}/$TARGET/lib32/libpthread.a
fi

echo "* ==============================================================="
echo "* building target ${TARGET}"
echo "* ==============================================================="
sleep 1
mkdir -p ${PKG}${MINGWROOT}/$TARGET/lib
RC="$TARGET-windres -F pe-x86-64" \
DLLTOOL="$TARGET-dlltool -m i386:x86-64" \
../$SRCNAM-code-$GITVERSION/configure \
  --target=$TARGET \
  $DEFOPTS

make -j1 realclean GC ARCH="-m64"
cp -a libpthreadGC3.dll.a pthreadGC3.dll ${PKG}${MINGWROOT}/$TARGET/lib
mv ${PKG}${MINGWROOT}/$TARGET/lib/libpthreadGC3.dll.a ${PKG}${MINGWROOT}/$TARGET/lib/libpthread.dll.a
mv ${PKG}${MINGWROOT}/$TARGET/lib/pthreadGC3.dll ${PKG}${MINGWROOT}/$TARGET/lib/pthread.dll

make -j1 realclean GC-static ARCH="-m64"
cp -a libpthreadGC3.a ${PKG}${MINGWROOT}/$TARGET/lib
mv ${PKG}${MINGWROOT}/$TARGET/lib/libpthreadGC3.a ${PKG}${MINGWROOT}/$TARGET/lib/libpthread.a

mkdir -p ${PKG}${MINGWROOT}/$TARGET/include
cd $WRK/$SRCNAM-code-$GITVERSION
cp -a pthread.h sched.h semaphore.h _ptw32.h ${PKG}${MINGWROOT}/$TARGET/include
chmod 0644 ${PKG}${MINGWROOT}/$TARGET/include/*

mkdir -p $PKG/usr/doc/$PKGNAM-$VERSION
cd $WRK/$SRCNAM-code-$GITVERSION
cp \
  ANNOUNCE BUGS CONTRIBUTORS ChangeLog FAQ LICENSE NEWS NOTICE PROGRESS README* TODO WinCE-PORT \
  $PKG/usr/doc/$PKGNAM-$VERSION/
cat $CWD/$PKGNAM.SlackBuild >$PKG/usr/doc/$PKGNAM-$VERSION/$PKGNAM.SlackBuild

# Add slack-desc:
mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
sed -e "s:@MINGWROOT@:${MINGWROOT}:g" -e "s:@TARGET@:${TARGET}:g" $CWD/doinst.sh >$PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PKGNAM-$VERSION-$ARCH-${BUILD}${TAG}.${PKGTYPE}

echo "* cleaning up"
cd $WRK; rm -rf $SRCNAM-* $PKG
