#!/bin/bash

fixlink32() {
  for L in $(find ./ -type l)
  do
    T1=$(readlink $L)
    if echo "$T1" | grep -q -R '(bin|sbin|libexec)/'; then
      T2=$(echo "${T1}" | sed -e 's,bin/,bin/32/,g' -e 's,libexec/,libexec/32/,g')
      rm -f "$L"
      ln -s "$T2" "$L"
    fi
  done
}

if [ ! -r ./repack32.list ]; then
  echo "ERROR: missing repack32.list"
  exit 1
fi

. ./repack32.list

WRK="/tmp/repack32"
mkdir -p $WRK
cd $WRK

for PKGNAM in ${PKGLIST[@]}; do
  PKGDIR="${PKGNAM}-compat32"
  PKG32="$(ls ${SRCDIR}/*/${PKGNAM}-*.t?z)"
  if [ ! -f "$PKG32" ]; then
    echo "ERROR: missing package $PKG32"
    exit 1
  fi
  VERSION=$(echo "$PKG32" | rev | cut -d '-' -f 3 | rev)
  BUILD=$(echo "$PKG32" | rev | cut -d '-' -f 1 | cut -d '.' -f 2 | rev)

  if [ ! -f "/var/lib/pkgtools/packages/${PKGNAM}-compat32-${VERSION}-x86_64-${BUILD}_wls" ]; then
    rm -rf $PKGDIR
    mkdir -p $PKGDIR
    cd $PKGDIR

    explodepkg $PKG32

    rm -rf ./usr/{adm,dict,doc,games,include,info,man,share,spool,src,tmp,X11,X11R6}
    rm -rf ./var ./etc

    for d in bin sbin libexec; do
      if [ -d ./$d ]; then
        mv ./$d ./${d}32
        mkdir -p ./$d
        mv ./${d}32 ./$d/32
        ( cd ./$d/32; fixlink32; )
      fi
      if [ -d ./usr/$d ]; then
        mv ./usr/$d ./usr/${d}32
        mkdir -p ./usr/$d
        mv ./usr/${d}32 ./usr/$d/32
        ( cd ./usr/$d/32; fixlink32; )
      fi
    done

    grep "${PKGNAM}:" install/slack-desc | sed "s/${PKGNAM}:/${PKGNAM}-compat32:/g" >install/slack-desc32
    cat install/slack-desc32 >install/slack-desc
    rm -f install/slack-desc32

    /sbin/makepkg -p -c n -l y ../${PKGNAM}-compat32-${VERSION}-x86_64-${BUILD}_wls.tlz

    cd $WRK
    /sbin/upgradepkg --install-new --reinstall ${PKGNAM}-compat32-${VERSION}-x86_64-${BUILD}_wls.tlz
    sleep 1
  else
    echo "* SKIP: package ${PKGNAM}-compat32-${VERSION}-x86_64-${BUILD}_wls already installed"
  fi
done

P="/etc/profile.d/build32.sh"
if ! [ -r "$P" ]; then
  echo 'export PATH="$PATH:/usr/sbin/32:/sbin/32:/usr/bin/32:/bin/32"' >>"$P"
  echo 'export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/lib/pkgconfig:/lib/pkgconfig"' >>"$P"
else
  if ! grep -q "PATH=" "$P"; then
    echo 'export PATH="$PATH:/usr/sbin/32:/sbin/32:/usr/bin/32:/bin/32"' >>"$P"
  elif ! grep -q "PKG_CONFIG_PATH=" "$P"; then
    echo 'export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/lib/pkgconfig:/lib/pkgconfig"' >>"$P"
  fi
fi
