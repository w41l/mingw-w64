# Prepartion

Add symlink for gcc-{ar,nm,ranlib} if those were not exists yet:
```
cd /usr/bin
ln -s gcc-ar $ARCH-slackware-linux-ar
ln -s gcc-nm $ARCH-slackware-linux-nm
ln -s gcc-ranlib $ARCH-slackware-linux-ranlib
```


# Building and installation order

* mingw-w64-binutils: build and install
* mingw-w64-headers: build and install
* mingw-w64-headers-bootstrap (created when making mingw-w64-headers): install
* mingw-w64-gcc-static: build and install
* mingw-w64-crt: build and install
* mingw-w64-libgcc-static: build and install
* mingw-w64-(winpthreads or pthreads4w): build and install
* mingw-w64-gcc: build
* mingw-w64-gcc: install
* mingw-w64-gcc-static: removepkg
* mingw-w64-libgcc-static: removepkg
* mingw-w64-headers-bootstrap: removepkg


# Tarball source code repository

* GNU Binutils: https://mirrors.slackware.com/slackware/slackware64-current/source/d/binutils/
* GNU GCC: https://mirrors.slackware.com/slackware/slackware64-current/source/d/gcc/
* MingW-W64: https://downloads.sourceforge.net/project/mingw-w64/mingw-w64/mingw-w64-release/mingw-w64-v12.0.0.tar.bz2
* Pthreads4w: https://sourceforge.net/projects/pthreads4w/files/pthreads4w-code-v3.0.0.zip

# Pre-compiled packages (OLD PACKAGES)

You can download my pre-compiled mingw-w64 packages here:

https://drive.google.com/open?id=1M7g46cUialNgUSuLBgtb8w2UoCssT38J

Please read the README.txt before installing my mingw-w64 packages.


# About bootstrap packages

These two packages only used in building process. You can safely removepkg
these packages after installing other packages:

* mingw-w64-headers-bootstrap
* mingw-w64-gcc-static
* mingw-w64-libgcc-static
